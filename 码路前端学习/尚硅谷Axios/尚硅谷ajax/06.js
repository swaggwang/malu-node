//1、引入express
const express=require('express')

//2、创建应用对象
const app=express()

//3、创建路由规则
app.get('/json-server',(request,response)=>{
    //设置相应头,设置允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    //设置一个json数据
    let data={
        name:'nailin'
    }
    let str=JSON.stringify(data)
    //设置响应体
    response.send(data)
})
//json响应
app.post('/json-server',(request,response)=>{
    //设置响应头,设置允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    //设置一个json数据
    let data={
        name:'nailin'
    }
    //设置响应体
    response.send('hello post')
})
//4、监听端口启动服务
app.listen(8000,()=>{
    console.log("服务已启动,页面06,8000端口监听中");
})