let koa=require('koa')
let Router=require('@koa/router')
let bodyparser=require('koa-bodyparser')
let cors=require('koa2-cors')
const bodyParser = require('koa-bodyparser')
let path=require('path')
let fs=require('fs')
const { error } = require('console')

let router=new Router()
let paths=path.join(__dirname,'../data/book.json')

router.prefix('/book')
router.get('/',ctx=>{
    let data=require('../data/book.json')
    ctx.body={
        code:1,
        data:data,
    }
})

// 添加数据
router.post('/add',async ctx=>{
    let body=ctx.request.body
    let data=require('../data/book.json')

    // id 由后端生成的每个人的唯一标识符 
    body.id = +data[data.length -1 ].id + 1

    //往json文件更新改动后的数据
    data.push(body)
    let newData = JSON.stringify(data)
    let result = await new Promise((resolve,reject) => {
        fs.writeFile(paths, newData, (error,result)=>{
            if(error){
                reject(error)
                throw error
            }
            resolve({
                status: 1,
                msg: "添加成功"
            })
        })
    })
   ctx.body = result
})

// 删除数据
router.post('/del',async ctx=>{
    // 根据id删除
    let id=ctx.request.body.id
    let data=require('../data/book.json')

    // 执行删除 如果没有id提示用户刷新页面
    let index=data.findIndex(item=>id==item.id)
    if (index!=-1) {
        // 找到了id对应的数据
        data.splice(index,1)

        //往json文件更新改动后的数据
        let newData=JSON.stringify(data)
        let result=await new Promise((resolve,reject)=>{
            fs.writeFile(paths,newData,(error,result)=>{
                if(error){
                    reject(error)
                    throw error
                }
                resolve({
                    status: 1,
                    msg: "删除成功"
                })
            })
        })
        ctx.body = result

    }else{
        // -1 找不到此数据
        ctx.body={
            code:0,
            msg:'找不到此数据'
        }
    }
})

// 修改数据
router.post('/update',async ctx=>{
    // 获取参数  修改数据 根据id查找数据
    let body=ctx.request.body

    let data=require('../data/book.json')

    // 根据id找到此条数据
    // data.forEach((item,index)=>{
    //     if (body.id==item.id) {
    // 修改这条数据 为body传递过来的数据
    //         data[index]=body
    //     }
    // })
    let index=data.findIndex(item=>item.id==body.id)
    // 有数据 根据下标修改数据
    if (index!=-1) {
        data[index]=body

        //往json文件更新改动后的数据
        let result=await new Promise((resolve,reject)=>{
            fs.writeFile(paths,newData,(error,result)=>{
                if (error) {
                    reject(error)
                    throw error
                }
                resolve({
                    code:1,
                    msg:'修改成功'
                })
            })
        })
        ctx.body=result

    }else{
        // -1 找不到此数据
        ctx.body={
            code:0,
            msg:'找不到此数据'
        }
    }
})

module.exports=router