// koa npm i koa 
// router npm i @koa/router
// koa-body  koa-bodyparser  前者可以解析文件， 后者不能 
// npm i koa-bodyparser
// npm i koa2-cors 
const Koa = require('koa')
const Router = require('@koa/router')
const bodyparser = require('koa-bodyparser')
const cors = require('koa2-cors')
const path = require('path')
const fs = require('fs')
// 登录注册接口数据来源  json 
const paths = path.join(__dirname,'./data/user.json')

// 引入book模块 
const book = require('./route/book')
const app = new Koa()
const router = new Router()


app.use(bodyparser())
app.use(cors())
// 登录
router.post('/login', ctx=>{
    // 接收传参 
    let body = ctx.request.body
    let data = require('./data/user.json')
    // 查找有没有这个人 
    // 数组方法  fliter 将满足条件的数据筛选出来  格式 数组类型的数据 
    //          find 查找方法 返回一个对象， 
    //          findIndex 返回一个下标   如果找到了有下标，如果找不到 就是-1
    let index = data.findIndex(item => item.username == body.username)
    if(index !=-1){
        // 证明有这个人    接下来判断密码是否正确
        if(data[index].password == body.password){
            // 登录成功
            ctx.body = {
                status: 1,
                msg: '登陆成功'
            }
        }else{
            ctx.body = {
                status: 0,
                msg: '密码错误'
            }
        }
        
    }else{
        ctx.body = {
            status: 0,
            msg: "暂无此账号，请先去注册"
        }
    }
})

// 注册
router.post('/regist', async ctx=>{
    let body = ctx.request.body
    let data = require('./data/user.json')
    // id 由后端生成的每个人的唯一标识符 
    body.id = +data[data.length -1 ].id + 1
    body.level = 1
    data.push(body)
    let newData = JSON.stringify(data)
    let result = await new Promise((resolve,reject) => {
        fs.writeFile(paths, newData, (error,result)=>{
            if(error){
                reject(error)
                throw error
            }
            resolve({
                status: 1,
                msg: "注册成功"
            })
        })
    })
    ctx.body = result
})

// 校验接口
router.post('/check', ctx =>{
    // 检测用户名是否存在
    let body = ctx.request.body
    let data = require('./data/user.json')
    let index = data.findIndex(item => item.username == body.username)
    if(index == -1){
        // 证明找不到同名的用户  可以注册
        ctx.body = {
            status: 1,
            msg: "账号可以注册"
        }
    }else{
        // 用户已存在
        ctx.body = {
            status: 0,
            msg: "用户已存在"
        }
    }
})

// 注册路由
app.use(book.routes())
book.allowedMethods()

app.use(router.routes())
app.listen(3000)
