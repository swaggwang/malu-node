// 接口调用都在这里调用接口  函数 结果都return出去
let baseUrl='http://localhost/book'
// 获取列表数据
function getlist(){
    // 发送请求
    return $.get(baseUrl)
}

// 添加数据的接口函数
function addBook(data){
    return $.post(baseUrl+'/add',data)
}

// 删除数据的接口函数
function delBook(data) {
    return $.post(baseUrl+'/del',data)
}

// 更新数据接口
function updateBook(data) {
    return $.post(baseUrl+'/update',data)
}

// 分页数据
function getPageList(data) {
    return $.post(baseUrl+'/pageData',data)
}