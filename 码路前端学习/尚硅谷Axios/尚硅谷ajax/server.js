//1、引入express
const express=require('express')

//2、创建应用对象
const app=express()

//3、创建路由规则
app.get('/json-server',(request,response)=>{
    //设置相应头,设置允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    //设置一个json数据
    let data={
        name:'nailin'
    }
    let str=JSON.stringify(data)
    //设置响应体
    response.send(str)
})
app.post('/json-server',(request,response)=>{
    //设置响应头,设置允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    //设置一个json数据
    let data={
        name:'nailin'
    }
    //设置响应体
    response.send('hello post')
})

//针对ie缓存
app.get('/ie',(request,response)=>{
    //设置响应头,设置允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    //设置响应体
    response.send('hello ie')
})

//延时响应
app.get('/delay',(request,response)=>{
    response.setHeader('Access-Control-Allow-Origin','*')
    setTimeout(()=>{
        response.send('延时响应')
    },3000)
    
})

//jQuery响应
app.all('/jquery-server',(request,response)=>{
    //设置响应头，设置允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    
    //响应头
    response.setHeader('Access-Control-Allow-Headers','*')
    let data={
        name:'乃琳'
    }
    //设置相应体
    response.send(JSON.stringify(data))   
})

//axios响应
app.all('/axios-server',(request,response)=>{
    //设置响应头，设置允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    //响应头
    response.setHeader('Access-Control-Allow-Headers','*')
    let data={
        name:'乃琳'
    }
    //设置相应体
    response.send(JSON.stringify(data))   
})

//fetch响应
app.all('/fetch-server',(request,response)=>{
    //设置响应头，设置允许跨域
    response.setHeader('Access-Control-Allow-Origin','*')
    //响应头
    response.setHeader('Access-Control-Allow-Headers','*')
    let data={
        name:'乃琳'
    }
    //设置相应体
    response.send(JSON.stringify(data))   
})
//4、监听端口启动服务
app.listen(8000,()=>{
    console.log("服务已启动,sever,8000端口监听中");
})