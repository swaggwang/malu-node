let express=require('express')
let app=express()
app.get('/home',(request,response)=>{
    response.sendFile(__dirname+'/index.html')
})
app.get('/data',(request,response)=>{
    response.send('用户数据')
})

app.all('/check-username',(request,response)=>{
    let data={
        exist:1,
        msg:'用户名已经存在'
    }
    //将数据转换成字符串
    let str=JSON.stringify(data)
    response.end(`handle(${str})`)
})
app.all('/jquery-jsonp-server',(request,response)=>{
    let data={
        name:'nailin',
        city:['枝江','向晚']
    }
    //将数据转换成字符串
    let str=JSON.stringify(data)
    //接收callback参数
    let cb=request.query.callback
    response.end(`${cb}(${str})`)
})
app.listen(9000,()=>{
    console.log('服务已启动');
})