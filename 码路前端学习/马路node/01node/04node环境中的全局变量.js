/* 
之前学习的全局变量 window 全局变量
alert confirm都是window上的
node里面没有window
*/
//__dirname 得到的是文件夹路径
console.log(__dirname);  
//__filename 当前文件路径
console.log(__filename);
/* 
process对象 全局对象在任何地方都能访问到它。通过对这个对象提供的属性和方法，使我们对当前的程序进行访问和控制
提供了node中进程相关的信息 
console.log(process);
*/
//nextTick方法 是微任务
console.log(process.nextTick);
//argv属性。返回一个数组，这个数组包含了Node.js中进程的命令行参数  数组第一个，返回启动node进程的可执行文件所在的绝对路径 数组第二个 当前执行js文件路径 数组后面的内容后为自己在终端里写的参数 
console.log(process.argv);
