// 模块分三类：
// 1、系统模块 node中自带的模块。导入系统模块，直接写模块名
// let http=require('http') 其他的还path url
// 2、第三方模块。一些大牛自己写的模块，使用别人写的 需要下载别人的模块，使用npm i来下载
// 3、自己写的模块。程序员自己写的模块 a.js
// 如果是自己写的模块，必须以./或者../打头


/* 
使用远程node去搭建服务器
需要使用一个模块，需要导入
*/
let http=require('http')
// 准备服务器实例（服务器提供数据，前端操作数据）
let server=http.createServer(function(req,res) {
    // 第一个参数 代表的请求
    // 第二个参数 代表着响应
    console.log(req,res);
    res.write('<h1>hello node</h1>');
    res.end()
})

//本地服务需要监听一个端口,端口号一般是在一个返回内
server.listen(3000,function(){
    console.log('服务器启动');
})

//监听事件的请求
server.on('request',function(){
    console.log('请求成功了');
})

//端口号返回 3000-8000
// 启动服务器
// 127.0.0.1:3000
// ip v4地址加端口号也可访问