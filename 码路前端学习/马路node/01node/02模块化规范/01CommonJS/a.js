/* 
一个js文件，就是一个模块
模块天生与世隔绝。铜墙铁壁 
导出 module.exports={a}
导入 require() 得到对象
*/
let name='nailin'
let age=18

//exports本身就是一个空对象
//导出就是，给这个对象上挂载一些属性
//挂载的属性，就是需要导出的属性
exports.name=name
//想让其他模块使用name

