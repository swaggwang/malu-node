//commonJS规范
/* 
exports 本质实际上是借用了module.exports的功能
module.exports才是我们导出的对象 是正主
exports实际上是浅拷贝了module.exports
exports={

}
module.exports={
    
}

模块分三类：
1、系统模块 node中自带的模块。导入系统模块，直接写模块名
let http=require('http') 其他的还path url
2、第三方模块。一些大牛自己写的模块，使用别人写的 需要下载别人的模块，使用npm i来下载
3、自己写的模块。程序员自己写的模块 a.js
如果是自己写的模块，必须以./或者../打头
*/

// ./或者../必须写,后缀.js可以不写
let a=require("./a.js")
console.log(a.name);

//可以解构出exports对象中的属性
let {PI,formatTime,sum}=require('./untils.js')
console.log(PI);
console.log(formatTime());
console.log(sum(1,2));

/* let admin=require('./主页')
let login=require('./登录退出')
admin.adminAdd()
admin.adminDelete()
admin.adminList()
admin.adminUpdate()
login.login()
login.logout() */