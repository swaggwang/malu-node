//node中使用的模块化是commonjs，es6中使用的模块化是ESmodule

//在ESmodule中导入是通过import xxx from xxx
//如果是自定义模块，必须以./或../打头
// {}不是解构赋值 {}中放标识符
// import {} from "./a"
//
import { name,age,love } from "./a.js";
import {age as bage} from './b.js'
console.log(name,age,love);
console.log(bage);

