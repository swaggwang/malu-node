// 每一个文件都是一个模块，模块和模块天生隔绝
//要让别人使用数据，需要导出

//在ESmodule中，通过export导出数据
// 导出方案一
// 导出去的并不是一个对象
// 导进去的也不是一个对象，导进去的就是一个标识符{}
export let name='nailin'
export let age=18
export function love() {
    console.log('乃琳是我老公');
}