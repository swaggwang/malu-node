// {}不是对象，也不是解构赋值，{}中放标识符列表
// import {name,age,love} from './a.js';
// console.log(name,age);
// love()

//如果给标识符起别名,只能使用别名
import {name as uname,age as uage,love} from './a.js';
console.log(uname);
console.log(uage);