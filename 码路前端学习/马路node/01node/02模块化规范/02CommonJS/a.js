let name='nailin'
let age=18
function sayHello() {
    console.log('hello');
}
//module是模块的意思。module.exports是正儿八经导出数据的
//exports.xx=xx 不是正儿八经导出数据的

//导出方案一：开发中不常用
// exports.name=name
// exports.age=age

//导出方案二：module.exports
// 源码中：exports和module.exports指向了同一个堆
module.exports.name=name
module.exports.age=age
module.exports.sayHello=sayHello

// 导出方案三:开发中常用
//让module.exports指向了新堆
// module.exports={
//     name,
//     age,
//     sayHello
// }
//exports还是指向老堆，此时，module.exports和exports没有关系了
// console.log(exports);  //{}

// 问：如下的导出是否ok?
// exports(地址)=module.exports(地址)
// 答：不ok，因为正经导出数据是通过module.exports
// 如果让exports指向新堆，数据是没有办法导出去的
// exports={
//     name,
//     age,
//     sayHello
// }