let koa=require('koa')
let log=require('koa-logger')
let onerror=require('koa-onerror')
let {accessLogger,logger}=require('./logger/index.js')
let app=new koa()

//控制台打印日志
// app.use(log())  打印到控制台以后在浏览器network中就不显示了
//错误处理中间件
onerror(app)
//持久化日志 一个正确日志，一个错误日志
app.use(accessLogger())

// app.use(async ctx=>{
//     ctx.body='hello'
// })

app.use(async (ctx,next)=>{
    ctx.throw(401,'未授权',{data:'乃琳是我老公'})
})

app.on('error',(err)=>{
    //利用logger.error就可以把错误处理持久化本地
    logger.error(err)
})

// app.use(async (ctx,next)=>{
//     // let err=new Error('未授权，不能访问')
//     // err.status=401
//     // throw err
    
//     // koa-onerror里的方法
//     ctx.throw(401,'未授权，不能访问',{data:'你需要登录后才能访问'})
// })

app.listen(3000,()=>{
    console.log('乃琳是我老公');
})