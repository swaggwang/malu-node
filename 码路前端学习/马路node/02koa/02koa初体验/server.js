// koa框架 人家写好的东西 需要安装依赖
// koa:npm i koa@2.13.4 -S

// -S 安装包会在package中的dependncies对象中
// -D 安装包会在package中 

//koa是第三方框架
// 第一步 koa
// npmjs.com 可以去官网中搜索写法

//引入
let Koa=require('koa')
// 因为koa是一个构造器 所以还需要创建一个实例化对象
// 2、创建实例
let app=new Koa()

// 3、配置响应
//下面这个整体叫中间件 里面的函数会自动执行
//use
app.use(ctx=>{
    console.log('请求过来了');
})

// 4、监听端口
app.listen()