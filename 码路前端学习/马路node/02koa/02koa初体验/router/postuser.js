let Router=require('@koa/router')

//创建一个路由器对象
//router叫路由器 route叫路由
let router=new Router()

//前缀
router.prefix('/user')

router.post('/list',(ctx,next)=>{
    console.log(ctx.params.id);
    ctx.body='用户列表'
    
    // form表单获取参数
    // console.log(ctx.request.body);
    // let name=ctx.request.body.username
    // let password=ctx.request.body.password
})

// 1、安装：npm i koa-bodyparser@4.3.0
// 2、配置
//     const bodyParser = require('koa-bodyparser')
//     app.use(bodyParser());
// 3、通过ctx.request.body得到的post提交过来的参数

router.post('/add',(ctx,next)=>{
    //post传参，参数在请求体里
    console.log(ctx.request.body);  
    ctx.body='增加用户'
})

router.post('/delete',(ctx,next)=>{
    ctx.body='删除用户'
})

//导出路由器对象
module.exports=router