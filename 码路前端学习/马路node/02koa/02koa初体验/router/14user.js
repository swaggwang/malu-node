let Router=require('@koa/router')

//创建一个路由器对象
//router叫路由器 route叫路由
let router=new Router()

//前缀
router.prefix('/user')

router.get('/list',(ctx,next)=>{
    console.log(ctx.params.id);
    ctx.body='用户列表'
})

router.get('/add',(ctx,next)=>{
    console.log(ctx.query);  
    ctx.body='增加用户'
})

router.get('/delete',(ctx,next)=>{
    ctx.body='删除用户'
})

//导出路由器对象
module.exports=router