let Router=require('@koa/router')

//创建一个路由器对象
//router叫路由器 route叫路由
let router=new Router()

//前缀
router.prefix('/user')

//我们可以把数据传递给后端，get请求，有get请求的传递方式
//get请求传参有两种方式
//     1、params传参  /user/1， /user/2 ，/user/3  静态传参
//    /user/:id   动态传参 和上面的一样，只不过可以接收任意数字参数
    // 2、query传参 /user?name=wc&age=18
    // get传参,参数需要放到url中  post传参，参数放到请求体中
router.get('/list/:id',(ctx,next)=>{
    console.log(ctx.params.id);
    ctx.body='用户列表'
})

router.get('/add',(ctx,next)=>{
    console.log(ctx.query);  
    ctx.body='增加用户'
})

router.get('/delete',(ctx,next)=>{
    ctx.body='删除用户'
})

//导出路由器对象
module.exports=router