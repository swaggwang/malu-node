let Router=require('@koa/router')
let bouncer=require('koa-bouncer')

//创建一个路由器对象
//router叫路由器 route叫路由
let router=new Router()

router.post('/',async (ctx,next)=>{
    // console.log(ctx.request.body);
    try {
        ctx.validateBody('username').required('用户名是必传参数')
    } catch (err) {
        if(err instanceof bouncer.ValidationError){
            ctx.status=400
            ctx.body={
                ok:0,
                message:'校验失败'+err.message
            }
            return 
        }
        throw err
    }
})

router.get('/',(ctx,next)=>{
    ctx.body='欢迎访问首页面'
})

//导出路由器对象
module.exports=router