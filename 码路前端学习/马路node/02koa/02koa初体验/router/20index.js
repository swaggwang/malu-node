let Router=require('@koa/router')
let bouncer=require('koa-bouncer')
let captcha=require('trek-captcha')

//创建一个路由器对象
//router叫路由器 route叫路由
let router=new Router()

// 生成图形验证码
router.get('/captcha',async(ctx,next)=>{
    let {buffer,token}=await captcha({size:4})

    // token的作用:前端输入验证码与此token进行对比

    // 生成验证码图片
    ctx.body=buffer
})

router.get('/',(ctx,next)=>{
    ctx.body='欢迎访问首页面'
})

//导出路由器对象
module.exports=router