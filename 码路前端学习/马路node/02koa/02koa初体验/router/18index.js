let Router=require('@koa/router')
const multer = require('koa-multer')

//创建一个路由器对象
//router叫路由器 route叫路由
let router=new Router()

//配置上传文件
const storage = multer.diskStorage({
    // 文件保存路径
    destination: function(req, file, cb) {
        cb(null,'../public/upload')
    },
    // 修改文件名称
    filename: function(req, file, cb) {
        //  获取上传文件的后缀名
        const filterFormat = file.originalname.split('.')
        cb(null, Date.now() + '.' + filterFormat[filterFormat.length - 1])
    },
})

const upload = multer({
    storage:storage
})

router.get('/',(ctx,next)=>{
    ctx.body='欢迎访问首页面'
})

// avatar那自己设置在前端上传的文件名，在前端和upload.single('文件名')，文件名要一致
router.post('/upload', upload.single('avatar'), (ctx, next) => {
    // file 是avatar文件的信息
    // ctx.req.body 文本域的数据 如果存在
    console.log(ctx.req.file.filename)
    ctx.body = {
        ok: 1,
    }
})

//导出路由器对象
module.exports=router