let Router=require('@koa/router')

//创建一个路由器对象
//router叫路由器 route叫路由
let router=new Router()

//前缀
router.prefix('/user')

// 登录接口
router.post('/login',(ctx,next)=>{
    let {body}=ctx.request //body就是前端传递过去的数据
    console.log('body:',body);  //{user:nailin,pwd:"123"}
    ctx.session.userinfo=body.user //在session里存储,request进去的参数 user用户名

    // 后端API接口,一般都是响应JSON数据
    ctx.body={
        ok:1,
        message:'登录成功',
    }
})

//退出登录接口   登出
router.post('/logout',(ctx,next)=>{
    if (ctx.session.userinfo) {
        delete ctx.session.userinfo
    }
    ctx.body={
        ok:1,
        message:'退出成功',
    }
})

//获取用户信息接口
router.get('/getUser',require('../middleware/auth.js'),(ctx,next)=>{
    // if (ctx.session.userinfo) {
    //     ctx.body={
    //         ok:1,
    //         message:'获取用户信息成功',
    //         userinfo:ctx.session.userinfo,
    //     }
    // }else{
    //     ctx.body={
    //         ok:0,
    //         message:'获取用户信息失败',
    //     }
    // }

        ctx.body={
            ok:1,
            message:'获取用户信息成功',
            userinfo:ctx.session.userinfo,
        }
    
})

// 获取个人收藏数据
router.get('/getLike',require('../middleware/auth.js'),(ctx,next)=>{
    ctx.body={
        ok:1,
        message:'获取个人收藏数据成功',
    }
})

router.get('/list/:id',(ctx,next)=>{
    console.log(ctx.params.id);
    ctx.body='用户列表'
})

router.get('/add',(ctx,next)=>{
    console.log(ctx.query);  
    ctx.body='增加用户'
})

router.get('/delete',(ctx,next)=>{
    ctx.body='删除用户'
})

//导出路由器对象
module.exports=router