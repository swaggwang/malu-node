let Router=require('@koa/router')
let jwt=require('jsonwebtoken')
let jwtAuth=require('koa-jwt')
//创建一个路由器对象
//router叫路由器 route叫路由
let router=new Router()

//前缀
router.prefix('/user')

let secret='nailin'

//登录
router.post('/login-token',(ctx,next)=>{
    // 得到用户信息
    let userinfo = ctx.request.body.user
    
    // 根据用户信息生成token  需要自己手动生成一个令牌token
    let token=jwt.sign({
        data:userinfo,//不要存放敏感数据
        exp:Math.floor(Date.now()/1000)+60*60,
    },secret)
    // console.log('secret:',secret);

    ctx.body={
        ok:1,
        message:'登录成功',
        user:userinfo,
        token:token,
    }
})

//登出
router.post('/logout',(ctx,next)=>{
    
})

//用户信息
//在使用 JWT 进行身份验证时，通常会使用一个密钥 (secret) 来对 token 进行签名，以确保 token 的完整性和安全性。
//在服务器端，验证 JWT 的时候需要使用相同的密钥来验证 token 的签名是否合法。

//也就是说在使用 jwtAuth() 进行身份验证时，需要将密钥传递给这个函数，以便它在验证 JWT 时使用该密钥来验证 token 的签名
// 换句话说：使用 jwtAuth() 进行身份验证时，需要将 token 传递给该函数，然后该函数会解析 token 并使用 secret 进行验证

//jwtAuth({ secret }) 中的 secret 参数就是用来指定密钥的，它是一个必填参数。
//secret 不是 token，它是一个用于生成和验证 token 的密钥。
router.get('/getUser-token',jwtAuth({
    secret
}),async(ctx,next)=>{
    ctx.body={
        message:'获取用户令牌',
        userinfo:ctx.state.user.data
    }
})

// 第三方登录
// 短信登录  用的比较多

//导出路由器对象
module.exports=router

// 常见的鉴权方案
//     1、session鉴权
//     2、jwt+token鉴权
//     3、第三方登录()
//     4、短信登录
//     5、扫码登录