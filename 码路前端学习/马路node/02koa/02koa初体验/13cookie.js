// cookie和session都是用来记录数据的
//     cookie数据存储在浏览器中，session数据是存储在服务器端
// cookie
//     小王去喝咖啡，咖啡馆有活动 喝10杯，送1杯
//          积分卡  cookie是由服务器种植的
// session
//         小王去喝咖啡，咖啡馆有活动 喝10杯，送1杯
//         在咖啡馆记录信息 有小王的手机号
//         手机号会以cookie的形式种植到客户端  手机号还是保存到cookie中的
//         每一次去喝咖啡都需要报手机号
//         session是基于cookie的

// jwt+token
const Koa = require('koa')
const index = require('./router/index')
const users = require('./router/13user')
const bodyParser = require('koa-bodyparser')
const static = require('koa-static');

const app = new Koa()

app.use(bodyParser());

// 注册路由
app.use(index.routes())
index.allowedMethods()
app.use(users.routes())
users.allowedMethods()

app.listen(3000, () => {
    console.log('3000端口被监听了~~')
})