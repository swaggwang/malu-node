//框架
    // 学习规则：按人家的要求，在合适的位置，写合适的代码
let koa=require('koa')
let app=new koa()

//ctx 是上下文 
// use是中间件，客户端向浏览器发送请求 需要经过中间件
app.use(async ctx=>{
    //响应一个字符串，叫hello
    ctx.body='hello'
})

app.listen(3000)


