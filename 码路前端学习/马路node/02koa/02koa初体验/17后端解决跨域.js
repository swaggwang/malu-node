let koa=require('koa')
let cors=require('koa2-cors')

let app=new koa()

//使用koa2-cors模块解决跨域问题,写这一行代码就行了
app.use(cors())