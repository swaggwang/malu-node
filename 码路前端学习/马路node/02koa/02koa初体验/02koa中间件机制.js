//框架
    // 学习规则：按人家的要求，在合适的位置，写合适的代码
    let koa=require('koa')
    let app=new koa()
    
    app.use(async (ctx,next)=>{
        console.log(1);
        //一个next，就是下个中间件里的所有代码
        /* 
        现在这个next是下个中间件里的所有代码，并且里面还可以有next 接着套娃下下个中间件里的代码
        console.log(2);
        await next()
        console.log(4);
        */
        await next()
        console.log(5);
    })
    app.use(async (ctx,next)=>{
        console.log(2);
        await next()
        console.log(4);
    })
    app.use(async (ctx,next)=>{
        console.log(3);
        ctx.body='hello'
    })
    app.listen(3000)
    
    
    