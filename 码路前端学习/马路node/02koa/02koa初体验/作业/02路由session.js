let Router=require('@koa/router')
let router=new Router()

router.prefix('/user')

//登录
router.post('/login',(ctx,next)=>{
    let {body}=ctx.request
    ctx.session.userinfo=body.user
    ctx.body={
        ok:1,
        message:'登录成功',
    }
})

//登出
router.post('/logout',(ctx,next)=>{
    if (ctx.session.userinfo) {
        delete ctx.session.userinfo
    }

    ctx.body={
        ok:0,
        message:'退出成功',
    }
})

//获取用户信息
router.post('/getuser',require('../middleware/auth.js'),(ctx,next)=>{
    ctx.body={
        ok:1,
        message:'获取用户信息成功',
        userinfo:ctx.session.userinfo,
    }
})

//导出路由对象
module.exports=router