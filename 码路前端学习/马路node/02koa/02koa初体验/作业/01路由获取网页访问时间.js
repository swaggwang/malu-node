let Router=require('@koa/router')

//创建一个路由器对象
//router叫路由器 route叫路由
let router=new Router()

//前缀
router.prefix('/user')

router.get('/list',(ctx,next)=>{
    //获取每次访问的时间
    let last=ctx.cookies.get('last')
    ctx.cookies.set('last',new Date().toLocaleString(),{
        maxAge:3600000
    })
    if (last) {
        ctx.body=`上一次访问的时间是${last}`
    }else{
        ctx.body='这是你第一次访问本网站'
    }

})

router.get('/add',(ctx,next)=>{
    console.log(ctx.query);  
    ctx.body='增加用户'
})

router.get('/delete',(ctx,next)=>{
    ctx.body='删除用户'
})

//导出路由器对象
module.exports=router