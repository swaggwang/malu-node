const Koa = require('koa')
const session = require("koa-session");
const bodyParser=require('koa-bodyparser')
let user=require('./02路由session.js')
const app = new Koa()

//post请求获取数据
app.use(bodyParser())

// 对cookie进行加密签名
app.keys=['some secret hurr']

// 对session的配置
const SESSION_CONFIG={
    key: 'nailin', //设置cookie的key名字
    maxAge: 3600000, //有效期，默认是一天
    httpOnly: true, //仅服务端修改
    signed: true, //签名cookie
    secure:false,
}
app.use(session(SESSION_CONFIG,app))

// 注册路由
app.use(user.routes())
user.allowedMethods()

app.listen(3000, () => {
    console.log('启动')
})