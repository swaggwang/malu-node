const Koa = require('koa')
const users = require('./01路由获取网页访问时间.js')
const bodyParser = require('koa-bodyparser')
const static = require('koa-static');

const app = new Koa()

app.use(bodyParser());

// 注册路由
app.use(users.routes())
users.allowedMethods()

app.listen(3000, () => {
    console.log('启动')
})