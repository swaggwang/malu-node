let koa=require('koa')
let Router=require('@koa/router')
let bodyparser=require('koa-bodyparser')
let jwt=require('jsonwebtoken')
let jwyAuth=require('koa-jwt')

let app=new koa()
let router=new Router()

let secret='bella'
app.use(bodyparser())

//登录接口
router.post('/login',(ctx,next)=>{
    let userData=require('./user.json')
    let body=ctx.request.body
    // 声明变量去校验信息
    let flag='none'
    userData.forEach(item => {
        if (item.username==body.username) {
            if (item.password==body.password) {
                let token=jwt.sign({
                    data:body,
                    exp:Math.floor(Date.now())
                },secret) // 传递密钥给 jwt.sign 方法

                ctx.body={
                    status:'1',
                    msg:'登录成功',
                    token // 将 token 返回给客户端
                }
                flag='success'
            }else{
                flag='have'
                ctx.body={
                    status:'0',
                    msg:'密码错误',
                }
            }
        }
        if (flag==='none') {
            ctx.body={
                status:'0',
                msg:'无',
            }
        }
    });
})

router.get('/adminlist',jwyAuth({secret}),ctx=>{
    
})

app.listen(3000,()=>{
    console.log('启动');
})
