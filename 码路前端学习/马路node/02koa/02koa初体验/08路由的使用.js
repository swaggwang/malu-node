let koa=require('koa')
let index=require('./router/index.js')
let user=require('./router/user.js')

let app=new koa()

//注册首页模块路由
app.use(index.routes())
index.allowedMethods()

//注册用户模块路由
app.use(user.routes())
user.allowedMethods()

app.listen(3000,()=>{
    console.log('乃琳是我老公');
})