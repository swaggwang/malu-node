let koa=require('koa')
let logger=require('koa-logger')
let app=new koa()

app.use(logger())

//浏览器地址栏只能发送get请求
//a标签，img标签，link标签，script标签，只能发送get请求
// from表单中有一个method属性，如果method属性是get，表示发送get请求
// from表单中有一个method属性，如果method属性是post，表示发送post请求
// postman,也可以发送post请求
//ajax可以发送get请求，也可以发送post请求
app.use(async ctx=>{
    ctx.body='hello'
})

app.listen(3000,()=>{
    console.log('3000端口响应了');
})