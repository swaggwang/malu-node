export default{
    save(obj){
        sessionStorage.setItem('todomvc',JSON.stringify(obj))
    },
    get(){
        let todomvc=JSON.parse(sessionStorage.getItem('todomvc'))
        if (todomvc==null) {
            return{
                todos:[],
                visibility: 'all'
            }
        }else{
            return todomvc
        }
    }
}