import Vuex from 'vuex'
import Vue from 'vue'
import localStorage from '@/utils/localStorage'

Vue.use(Vuex)

let actions={

}

let mutations={
    addTodo(state,text){
        if (text.trim()=='') alert(输入的数组不能为空)
        state.todos.push({text:text,done:false})
        localStorage.save(state)
    },
    delTode(state,todo){
        let index=state.todos.findIndex(item=>item.text==todo.text)
        if (index!=-1) {
            state.todos.splice(index,1)
        }
        localStorage.save(state)
    },
    taggleTodo(state,todo){
        todo.done=!todo.done
        localStorage.save(state)
    },
    taggleAllTodo(state,flag){
        state.todos.forEach(item =>item.done=flag
        );
        localStorage.save(state)
    },
    delDone(state,todos){
        state.todos=todos.filters((item)=>item.done==false)
        localStorage.save(state)
    },
    changeVisibility(state,visibility){
        state.visibility=visibility
        localStorage.save(state)
    },
    editing(state,obj){
        obj.todo.text=obj.txt
        localStorage.save(state)
    }
}

let getters={
    isAllDone(state){
        return state.todos.every((item)=>item.done)
    },
    unDoneNumbe(state){
        let num=0
        state.todos.forEach(item=>{
            if (item.done==false) {
                num++
            }
        })
        return num
    },
    filterTodo(state){
        if(state.visibility == 'all'){
            return state.todos
        }else if(state.visibility == 'active'){
            return state.todos.filter(item => !item.done)
        }else if(state.visibility == 'completed'){
            return state.todos.filter(item => item.done)
        }
    }
}

export default new Vuex.Store({
    actions,
    mutations,
    state:localStorage.get(),
    getters,
})