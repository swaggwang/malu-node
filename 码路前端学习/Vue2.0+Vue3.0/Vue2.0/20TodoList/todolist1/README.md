# todolist1

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

##总结TodoList案例
    1、组件化编码流程:
        1、拆分静态组件:组件要按照功能点拆分,命名不要与html元素冲突
        2、实现动态组件:考虑好数据的存放位置,数据是一个组件在用,还是一些组件在用
            1、一个组件在用:放在组件自身即可。
            2、一些组件在用:放在他们共同的父组件上(状态提升)
        3、实现交互:从绑定事件开始

    2、props适应于:
        1、父组件==>子组件 通信(传递数据)
        2、子组件==>父组件 通信(传递数据) 要求父先给子一个函数
    3、使用v-model时切记:v-model绑定的值不能是props传过来的值,因为props是不可以修改的！
    4、props传过来的若是对象类型的值,修改对象中的属性时Vue不会报错,但不推荐这样做。

##组件自定义事件
        1、一种组件间通信的方式,适用于:子组件==>父组件
        2、使用场景:A是父组件,B是子组件,子组件想给父组件传数据,那么就要在父组件中给子子组件绑定自定义事件(事件的回调在父组件中)
        3、绑定自定义事件:
            1、第一种方式,在父组件中:
            <Demo @atguigu='test'/> 或
            <Demo v-on:atguigu='test'/>
            2、第二种方式,在父组件中
            <Demo ref='xxx'/>
            .....
            mounted(){
                this.$refs.xxx.$on('atguigu',this.test)
            }
            3、若想让自定义事件只触发一次,可以使用once修饰符,或$once方法
        4、触发自定义事件
            this.$emit('atguigu',数据)
        5、解绑自定义事件:
            this.$off('atguigu')
        6、组件上也可以绑定原生DOM事件,需要使用native修饰符
        7、注意:通过this.$refs.xxx.$on('atguigu',回调)绑定自定义事件时,回调要么配置在methods中,要么就用箭头函数,否则this指向会出问题

##全局事件总线(GlobalEventBus)
    1、一种组件间通信的方式,适用于任意组件间通信
    2、安装全局事件总线:
        new Vue({
            ......
            beforeCreate(){
                //安装全局事件总线,$bus就是当前应用的vm
                //就是在Vue的原型对象上添加一个名为$bus的数据,里面的内容是vm的所有东西。现在$bus就可以用$emit,$off,$on

                Vue.prototype.$bus=this 
            },
            ......
        })

    3、使用事件总线:
        1、接收数据:
        A组件想接收数据,则在A组件中给$bus绑定自定义事件,事件的回调留在A组件自身
        methods(){
            demo(data){
                .....
            }
        }
        .......
        mounted(){
            this.$bus.$on('绑定事件名',this.demo)
        }
        2、提供数据(触发事件)
        this.$bus.$emit('绑定事件名',数据)

    4、最好在beforeDestroy钩子中,用$off('事件名')去解绑当前组件所用到的事件

##消息订阅于发布
    1、一种组件间通信的方式,适用于任意组件间通信
    2、使用步骤:
        1、安装pubsub:npm i pubsub-js
        2、引入:import pubsub from 'pubsub-js'
        3、接收数据:A组件想接收数据,则在A组件中订阅消息,订阅的回调留着A组件自身
            methods(){
                demo(msgName,data){
                    默认第一个参数会传递事件名
                    ....
                }
            }
            .....
            mounted(){
                this.pid=pubsub.subscribe('事件名',this.demo) //订阅消息
            }
        4、提供数据:pubsub.publish('事件名',数据)
        5、最好在beforeDestroy钩子中,用
        Pubsub.unsubscribe(pid)去取消订阅

##nextTick
    1、语法:this.$nextTick(回调函数)
    2、作用:在下一次DOM更新结束后执行其指定的回调
    3、什么时候用:当改变数据后,要基于更新后的新DOM进行某些操作时,要在nextTick所指定的回调函数中执行。

##Vue封装的过渡与动画
    1、作用:在插入、更新或移出DOM元素时,在合适的时候给元素添加样式类名
    2、图示：
        微信
    3、写法:
        1、准备好样式：
            元素进入的样式
                1、v-enter：进入的起点
                2、v-enter-active:进入过程中
                3、v-enter-to：进入的终点
            元素离开的样式
                1、v-leave：离开的起点
                2、v-leave-active:离开过程中
                3、v-leave-to：离开的终点

        2、使用<transition></transition>包裹要过渡的元素,并配置name属性:
            要是name是xxx了,v-leave、v-enter里的v也要换成xxx

            <transition name='hello'>
                <h1 v-show='isShow'>泥蒿</h1>
            </transition>

        3、备注:若有多个元素需要过渡,则需要使用<transition-group></transition-group>
        且每个元素的要指定key值,要不然找不到里面的元素