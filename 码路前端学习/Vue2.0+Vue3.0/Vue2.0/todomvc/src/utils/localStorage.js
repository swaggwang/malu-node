export default{
    // 本地存储  一件事 存数据  取数据 
    save(obj){
        localStorage.setItem('todomvc',JSON.stringify(obj))
    },
    get(){
        let todomvc = JSON.parse(localStorage.getItem('todomvc'))
        console.log(todomvc);
        if(!todomvc){
            return {
                todos: [],
                visibility: 'all'
            }
        }else{
            return todomvc
        }
    }
}